<!-- #include file="cookies.asp" -->

<%

if Session("LoginStatus") <> "Validated" then
	Response.Redirect "index.asp"
else


Set DataConn = Server.CreateObject("ADODB.Connection")
DataConn.ConnectionTimeout = Session("DataConn_ConnectionTimeout")
DataConn.CommandTimeout = Session("DataConn_CommandTimeout")
DataConn.Open Session("DataConn_ConnectionString"), Session("DataConn_RuntimeUserName"), Session("DataConn_RuntimePassword")
Set data = Server.CreateObject("ADODB.Recordset")
Set data.ActiveConnection = DataConn
data.MaxRecords = 1
data.CursorType = 1
data.LockType = 3
data.Source = "ConfAccess"
data.Open

data.AddNew
data("UserName") = Left( userName, 49 )
data("UserCompany") = Left( userCompany, 49 )
data("UserEmail") = Left( userEmail, 49 )
'data("ContactFirstname") = Left( userPhone, 49 )
data("Status") = "Success"
data("StartDate") = Date()
data("StartTime") = Time()
data("EventName") = "Marathon Fund Investor Day 2020"
data("ClientName") = "Marathon Fund"
data.Update

data.Close
DataConn.Close
set data = nothing
set DataConn = nothing

Session("disableRegistration") = true
Session("enforceRegistration") = false
Session("autoRegistration") = false

%>

<html>

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Oppenheimer 30th Annual Healthcare Conference</title>
<link href="css/multiColumnTemplate.css" rel="stylesheet" type="text/css">
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<!--<link href="generic.css" rel="stylesheet" type="text/css">-->

<script language="javascript">

function checkWin(win) {
	var bPopBlocked = false;
	try {
		if (win == null) bPopBlocked = true;
		else if (typeof(win) == "undefined") bPopBlocked = true;
		else if (win.closed) bPopBlocked = true;
		else if (win.length < 0) bPopBlocked = true;
	}
	catch(e) {
		bPopBlocked = true;
	}
	if (bPopBlocked) alert(
	 "Please disable any pop-up blocking software in your browser\n" +
	 "while viewing the Lexington Partners Annual Meeting. Your pop-up blocker\n" +
	 "appears to be interfering with display of the presentation."
	);
}

var presWin = null;

function goToPres(whatShow){

	document.userdata.password.value = whatShow;

	if (!presWin || presWin.closed ){
		presWin = window.open("temp.html","presWin","toolbar=0,menu=0,scrollbars=0,resizable=1,location=0,width=1600,height=900");
		//setTimeout("checkWin(presWin)", 3000);
	}

	document.userdata.submit();
	presWin.focus();

}
</script>

</head>

<body leftmargin="0" topmargin="0" rightmargin="0" marginheight="0" marginwidth="0">
<div class="container">
  <header>
    <div class="primary_header">
      <h1 class="title">Oppenheimer 30th Annual Healthcare Conference</h1>
    </div>
    <nav class="secondary_header" id="menu">
      <ul>
        <li class="title-company">Company</li>
        <li class="title-ticker">Ticker</li>
        <li class="title-date">Participation Date</li>
        <li class="title-attendees">Managment Attendees</li>
        </ul>
    </nav>
  </header>
  
  <div class="row">
    <div class="rows clickable" href="#" onClick="goToPres('opco2020HC001');">
		<div class="inside-column-company">
        <h4>89Bio</h4>
		</div>
		<div class="inside-column-ticker">
		<h4>ETNB</h4>
		</div>
		<div class="inside-column-date">
		<h4>Wednesday, March 18</h4>
		</div>
		<div class="inside-column-attendees">
		<h4>Rohan Palekar, CEO; Ryan Martins, CFO</h4>
		</div>
    </div>
	  <div class="rows clickable" href="#" onClick="goToPres('opco2020HC002');">
		<div class="inside-column-company">
        <h4>Addus Home Care</h4>
		</div>
		<div class="inside-column-ticker">
		<h4>ADUS</h4>
		</div>
		<div class="inside-column-date">
		<h4>Tuesday, March 17</h4>
		</div>
		<div class="inside-column-attendees">
		<h4>Dirk Allison, President & CEO; Brian Poff, EVP & CFO</h4>
		</div>
		  
    </div>
	  <div class="rows clickable" href="#" onClick="goToPres('opco2020HC003');">
		<div class="inside-column-company">
        <h4>ADMA Biologics, Inc.</h4>
		</div>
		<div class="inside-column-ticker">
		<h4>ADMA</h4>
		</div>
		<div class="inside-column-date">
		<h4>Tuesday, March 17</h4>
		</div>
		<div class="inside-column-attendees">
		<h4>Adam Grossman, President & CEO; Brian Lenz, EVP & CFO</h4>
		</div>
		  
    </div>
	 <div class="rows clickable" href="#" onClick="goToPres('opco2020HC004');">
		<div class="inside-column-company">
        <h4>Aduro Biotech, Inc.</h4>
		</div>
		<div class="inside-column-ticker">
		<h4>ADRO</h4>
		</div>
		<div class="inside-column-date">
		<h4>Wednesday, March 18</h4>
		</div>
		<div class="inside-column-attendees">
		<h4>Steve Isaacs, Chairman, President & CEO; Andrea Van Elsas; Dimitry Nuyten, CMO</h4>
		</div>
		  
    </div>
	  <div class="rows clickable" href="#" onClick="goToPres('opco2020HC005');">
		<div class="inside-column-company">
        <h4>Aerie Pharmaceuticals, Inc.</h4>
		</div>
		<div class="inside-column-ticker">
		<h4>AERI</h4>
		</div>
		<div class="inside-column-date">
		<h4>Wednesday, March 18</h4>
		</div>
		<div class="inside-column-attendees">
		<h4>Vince Anido, Chairman & CEO; Rich Rubino, CFO</h4>
		</div>
		  
    </div>
	  <div class="rows clickable" href="#" onClick="goToPres('opco2020HC006');">
		<div class="inside-column-company">
        <h4>Agios Pharmaceuticals</h4>
		</div>
		<div class="inside-column-ticker">
		<h4>AGIO</h4>
		</div>
		<div class="inside-column-date">
		<h4>Tuesday, March 17</h4>
		</div>
		<div class="inside-column-attendees">
		<h4>Andrew Hirsch, CFO; Kendra Adams, VP IR; Holly Manning, Director, IR</h4>
		</div>
		  
    </div>
   
    <!--<div class="columns">
      <p class="thumbnail_align">&nbsp;</p>
      <h4>TITLE</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
    </div>
    <div class="columns">
      <p class="thumbnail_align">&nbsp;</p>
      <h4>TITLE</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud.</p>
    </div>-->
  </div>
  
  
  <footer class="secondary_header footer">
    <div class="copyright">&copy;2020 - <strong>Oppenheimer</strong></div>
  </footer>
</div>

<form action="/feynman/protoshow/validate.asp" method="post" name="userdata" target="presWin">
<% if Request.Cookies("NrsSingleLogin.ApplicationCookie") <> "" and Session("login_Email") <> "" then %>
<input type="hidden" name="username" size="30" value="<%=Session("mid2020userName")%>">
<input type="hidden" name="company" size="30" value="<%=Session("mid2020userCompany")%>">
<input type="hidden" name="email" size="30" value="<%=Session("mid2020userEmail")%>">
<!--<input type="hidden" name="uniqueid" size="30" value="<%=Session("mid2020userPhone")%>">-->
<input type="hidden" name="password" size="30" value="">
<% else %>
<input type="hidden" name="username" size="30" value="<%=Session("mid2020userName")%>">
<input type="hidden" name="company" size="30" value="<%=Session("mid2020userCompany")%>">
<input name="email" type="hidden" value = "user@ms.com">
<!--<input type="hidden" name="uniqueid" size="30" value="<%=Session("mid2020userPhone")%>">-->
<input type="hidden" name="password" size="30" value="">
<% end if%>
</form>
</body>

</html>
<%
Session("mid2020uniqueid") = userPhone
end if
%>